# Docker Day pre-requisites

## What you need
1. Windows 10 pro machine
2. Linux machine in Digital Ocean
3. Docker for windows
4. Visual Studio 2017 with docker support installation
5. Visual Studio Code
6. Node JS
7. Express JS
8. BitBucket Account or github
9. Docker Hub Account
10. Putty


## Docker Installation

1. Install Docker for Windows from here : [Install](https://docs.docker.com/docker-for-windows/install/)
2. Get the stable version 
3. follow all the steps until you get the message Docker is now up and running


## Visual Studio 2017
1. Install Visual Studio 2017 from here : [Install](https://www.visualstudio.com/downloads/?rr=https%3A%2F%2Fwww.google.co.il%2F)
2. Make sure you install the .Net Core cross-platform workload
3. Make sure you install the ASP.net and web development workload

## Visual Studio Code
1. Install Visual Studio code from here : [Install](https://code.visualstudio.com/)
2. Browse the extensions and install as you wish (no mandatory)


## Intall Node.JS
1. Install Node.JS from here : [Install](https://nodejs.org/dist/v7.7.4/node-v7.7.4-x64.msi)
2. After installtion run open terminal: node -v and check the version
3. run : npm -v and check the version

## Install Express JS
1. open terminal and run : $ npm install express -g

## BitBucket or git hub
1. Bitbucket [sign-up](https://bitbucket.org/account/signin/) 
2. Github [sing-up](https://github.com/join)


## Create Docker Hub user 
1. Create Docker hub account here : [https://hub.docker.com/](https://hub.docker.com/)
2. Write down your credentails - you will need it for docker operations



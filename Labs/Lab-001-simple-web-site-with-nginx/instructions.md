# Simple Web site with Nginx

1. Create Index.html file (see attached)
2. Create Dockerfile (see attached)
3. Build Image :  docker build . -t liornabat/simple-website-nginx
4. Run the container : docker run -d --name simple-web -p 18000:80 liornabat/simple-website-nginx
5. Check your browser [localhost:18000] (localhost:18000)
6. check : docker ps  -- this show the container
7. check : docker Image ls -- this give the list of images
8. push the image to repository -- docker push liornabat/simple-website -- pay attention to the repository name
9. Kill the container : docker kill simple-web
10. Remove the container :docker rm simple-web
11. Change the Index.html
12. build again
13. run again
14. push again



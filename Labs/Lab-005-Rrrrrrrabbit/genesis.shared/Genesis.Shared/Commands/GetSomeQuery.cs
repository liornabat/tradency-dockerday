﻿using System;
using System.Collections.Generic;
using System.Text;
using Genesis.Common.Commands;
using Genesis.Common.Requests;
using System.Linq;

namespace Genesis.Shared.Commands
    {
    public class GetSomeQuery : IServiceQueryRequest
    {
        public Request Request { get; set; }
        public string QueryName { get; set; }
        public string From { get; set; }
        public string To { get; set; }

        public IEnumerable<string> Validate()
        {

            if (!QueryName.Any())
            {
                yield return ("Asset item must have a name");

            }
            if (!From.Any())
            {
                yield return ("Asset item must have an Exchange Id");

            }
            if (To.Any())
            {
                yield return ("Asset item must have a Type");

            }


        }
    }
}

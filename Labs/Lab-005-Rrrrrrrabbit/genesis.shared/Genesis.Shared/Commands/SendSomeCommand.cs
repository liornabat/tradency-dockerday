﻿using System;
using System.Collections.Generic;
using System.Text;
using Genesis.Common.Commands;
using Genesis.Common.Requests;
using System.Linq;

namespace Genesis.Shared.Commands

{
    public class SendSomeCommand : IServiceCommandRequest
    {
        public Request Request { get; set; }
        public string CommandName { get; set; }
        public string From { get; set; }
        public string To { get; set; }

        public SendSomeCommand()
        {
            CommandName = string.Empty;
            From = string.Empty;
            To = string.Empty; 
        }

        public IEnumerable<string> Validate ()
        {

            if (!CommandName.Any())
            {
                yield return ("Execute Command must have a value,");

            }
            if (!From.Any())
            {
                yield return ("From must have a value,");

            }
            if (!To.Any())
            {
                yield return ("To must have a value,");

            }
           

        }
    }
}

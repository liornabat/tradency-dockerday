﻿using Genesis.Common.Commands;

namespace Genesis.Shared.Commands
{
    public class CommandRequest2 : ICommand
    {
        public Request Request { get; set; }
        public string CommandName { get; set; }
        public string CommandData { get; set; }

    }
}

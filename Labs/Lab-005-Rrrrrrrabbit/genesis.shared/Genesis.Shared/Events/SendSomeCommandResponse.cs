﻿using Genesis.Common.Responses;
using Genesis.Common.Commands;
using Genesis.Common.Events;
using System;

namespace Genesis.Shared.Events
{
    public class SendSomeCommandResponse :IRequestResponse,IEvent
    {
        public Request Request { get ; set; }
        public ICommand  Command { get; set; }
        public string SomeResponseData { get; set; }
        public string SomeResponseData2 { get; set; }

        public SendSomeCommandResponse(ICommand command, string someResponseData, string someResponseData2)
        {
            Request = new Request();
            
            Command = command;
            Request.RefRequest = command.Request;
            SomeResponseData = someResponseData;
            SomeResponseData2 = someResponseData2;
        }
    }
}

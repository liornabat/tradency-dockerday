﻿using System.Collections.Generic;

namespace Genesis.Common.Validation
{
    public interface IValidator<in T>
    {
        IEnumerable<string> SetPropertiesAndValidate(T value);
    }
}
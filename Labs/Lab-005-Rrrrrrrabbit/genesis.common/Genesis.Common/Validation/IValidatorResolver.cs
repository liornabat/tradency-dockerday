﻿namespace Genesis.Common.Validation
{
    public interface IValidatorResolver
    {
        IValidator<T> Resolve<T>();
    }
}
﻿using System;

namespace Genesis.Common.Domain
{
    public abstract class TradencyException : Exception
    {
        public string Code { get; }

        protected TradencyException()
        {
        }

        protected TradencyException(string code)
        {
            Code = code;
        }

      
        protected TradencyException(string message, params object[] args) : this(string.Empty, message, args)
        {
        }

        protected TradencyException(string code, string message, params object[] args) : this(null, code, message, args)
        {
        }

        protected TradencyException(Exception innerException, string message, params object[] args)
            : this(innerException, string.Empty, message, args)
        {
        }

        protected TradencyException(Exception innerException, string code, string message, params object[] args)
            : base(string.Format(message, args), innerException)
        {
            Code = code;
        }
    }
}
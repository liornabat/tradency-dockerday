﻿using System;
using Genesis.Common.Commands;

namespace Genesis.Common.Events
{
    public interface IEvent
    {
        Request Request { get; set; }
    }
}
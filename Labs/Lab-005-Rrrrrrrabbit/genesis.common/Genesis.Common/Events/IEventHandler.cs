﻿using System.Threading.Tasks;

namespace Genesis.Common.Events
{
    public interface IEventHandler<in T> where T : IEvent
    {
        Task HandleAsync(T @event);
        
    }
}
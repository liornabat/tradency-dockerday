﻿using System;
using System.Collections.Generic;
using System.Text;
using Genesis.Common.Commands;

namespace Genesis.Common.Requests
{
    public interface IServiceQueryRequest : ICommand
    {
        string QueryName { get; set; }
        IEnumerable<string> Validate();
    }
}

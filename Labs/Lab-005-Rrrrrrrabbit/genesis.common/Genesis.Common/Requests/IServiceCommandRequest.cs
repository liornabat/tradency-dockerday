﻿using System;
using System.Collections.Generic;
using System.Text;
using Genesis.Common.Commands;

namespace Genesis.Common.Requests
{
    public interface IServiceCommandRequest : ICommand
    {
        string CommandName { get; set; }
        IEnumerable<string> Validate();
    }
}

﻿namespace Genesis.Common.Host
{
    public interface IResolver
    {
        T Resolve<T>();
    }
}
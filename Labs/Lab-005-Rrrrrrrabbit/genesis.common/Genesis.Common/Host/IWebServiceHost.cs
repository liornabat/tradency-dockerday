﻿
namespace Genesis.Common.Host
{
    public interface IWebServiceHost
    {
        void Run();
    }
}
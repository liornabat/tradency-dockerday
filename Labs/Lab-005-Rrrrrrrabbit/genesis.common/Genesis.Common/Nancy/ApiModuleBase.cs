﻿using System;
using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses;
using NLog;

namespace Genesis.Common.Nancy
{
    public abstract class ApiModuleBase : NancyModule
    {
        protected readonly IMapper Mapper;

        protected ApiModuleBase(string modulePath = "") 
            : base(modulePath)
        { }

        protected ApiModuleBase(IMapper mapper, string modulePath = "")
            : base(modulePath)
        {
            Mapper = mapper;
        }

      

        protected T BindRequest<T>() where T : new()
        => Request.Body.Length == 0 && Request.Query == null
            ? new T()
            : this.Bind<T>();

      
    }
}
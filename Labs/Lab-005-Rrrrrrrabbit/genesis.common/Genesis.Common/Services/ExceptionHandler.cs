﻿using System;

namespace Genesis.Common.Services
{
    public class ExceptionHandler : IExceptionHandler
    {

        public ExceptionHandler()
        {


        }

        public void Handle(Exception exception, params string[] tags)
        {

        }

        public void Handle(Exception exception, object data, string name = "Request details", params string[] tags)
        {
            
        }
        
    }
}
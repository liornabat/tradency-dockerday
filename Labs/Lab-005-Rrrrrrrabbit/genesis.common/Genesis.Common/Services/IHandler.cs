﻿using System;
using System.Threading.Tasks;

namespace Genesis.Common.Services
{
    public interface IHandler
    {
        IHandlerTask Run(Action action);
        IHandlerTask Run(Func<Task> actionAsync);
        void ExecuteAll();
        Task ExecuteAllAsync();
    }
}
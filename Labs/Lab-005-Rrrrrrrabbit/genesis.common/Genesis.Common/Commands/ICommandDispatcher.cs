﻿using System.Threading.Tasks;

namespace Genesis.Common.Commands
{
    public interface ICommandDispatcher
    {
        Task DispatchAsync<T>(T command) where T : ICommand;
    }
}
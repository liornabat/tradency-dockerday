﻿using System;


namespace Genesis.Common.Commands
{
    public class Request
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name { get; set; }
        public string Origin { get; set; }
        public string Tag { get; set; }
        public Request RefRequest { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
        public DateTime SentAt { get; set; }
        public DateTime ProccesedAt { get; set; }
        public DateTime CompletedAt { get; set; }
        public string Status { get; set; }
        public string Errors { get; set; }

        public static Request From<T>(Request request)
            => Create<T>(request.Id, request.Name, request.Origin, request.Tag);
        public static Request From<T>(Request request, Request refRequest)
            => Create<T>(request.Id, request.Name, request.Origin, request.Tag, refRequest);

        public static Request New<T>() => 
        
        new Request
        {
            Id = Guid.NewGuid(),
            Name = "",
            Origin = "",
            Tag = "",
            CreatedAt = DateTime.UtcNow
        };
    
        
        public static Request New<T>(Guid id) => Create<T>(id, string.Empty, string.Empty,string.Empty);
        public static Request New<T>(Guid id,Request refRequest) => Create<T>(id, string.Empty, string.Empty, string.Empty,refRequest);

        public static Request Create<T>(Guid id, string name ="",string origin ="",  string tag = "")
            => new Request
            { Id = id, 
                   Name = name, 
                   Origin = origin, 
                   Tag = tag, 
                   CreatedAt = DateTime.UtcNow };
        public static Request Create<T>(Guid id, string name = "", string origin = "", string resource = "", Request refRequest=null)
            => new Request
            {
                Id = id,
                Name = name,
                Origin = origin,
                Tag = resource,
                RefRequest = refRequest,
                CreatedAt = DateTime.UtcNow
            };
        public void SetSendStatus()
        {
            SentAt = DateTime.UtcNow;
            Status = "Sent";

        }
        public void SetProcesseStatus()
        {
            ProccesedAt = DateTime.UtcNow;
            Status = "Processed";

        }
        public void SetCompletedStatus()
        {
            CompletedAt = DateTime.UtcNow;
            Status = "Completed";

        }
        public void SetErrorStatus(string pStatus, string pErrors)
        {
            CompletedAt = DateTime.UtcNow;
            Status = pStatus;
            Errors = pErrors;

        }
    }
}
﻿
namespace Genesis.Common.Commands
{
    public interface ICommand
    {
        Request Request { get; set; }
    }
}
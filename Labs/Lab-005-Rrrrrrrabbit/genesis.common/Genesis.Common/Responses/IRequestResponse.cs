﻿using System;
using System.Collections.Generic;
using System.Text;
using Genesis.Common.Commands;
namespace Genesis.Common.Responses
{
    public interface IRequestResponse
    {
        ICommand Command { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Nancy.Owin;
using NLog.Web;
using NLog.Extensions.Logging;
using Genesis.Api.Framework;

namespace Genesis.Api
{
    public class Startup
    {

        public string EnvironmentName { get; set; }
        public IConfiguration Configuration { get; set; }


        public Startup(IHostingEnvironment env)
        {
            EnvironmentName = env.EnvironmentName.ToLowerInvariant();
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                //       .AddJsonFile($"appsettings.{EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables()
                .SetBasePath(env.ContentRootPath);

            //if (env.IsProduction())
            //{
            //    builder.AddLockbox();
            //}

            Configuration = builder.Build();
        }


        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddNLog();
            env.ConfigureNLog("nlog.config");

            app.UseOwin().UseNancy(x => x.Bootstrapper = new Bootstrapper(Configuration));
        }


    }
}

dotnet restore
dotnet build -c Release
dotnet publish -c Release -o bin/Release/PublishOutput
docker build . -t liornabat/genesis.api:latest
docker push liornabat/genesis.api:latest


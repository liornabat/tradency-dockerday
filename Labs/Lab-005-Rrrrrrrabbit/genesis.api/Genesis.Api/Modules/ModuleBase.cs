﻿using Nancy;
using Nancy.ModelBinding;
using NLog;
using Genesis.Common.Commands;
using Genesis.Common.Events;
using Genesis.Api.Framework;
using RawRabbit;
using System;


namespace Genesis.Api.Modules
{
    public abstract class ModuleBase :NancyModule
    {
        protected static readonly Logger Logger = LogManager.GetCurrentClassLogger();
      
        private readonly IBusClient _bus;
      

        public ModuleBase()
        {

        }


        protected ModuleBase( IBusClient bus, string modulePath="")
            :base(modulePath)
        {
            _bus = bus;
        }
      

        protected CommandRequestHandler<T> ForCommand<T>() where T :  ICommand, new()
        {
            if (Request.Headers.ContentType.Subtype != "json")
                throw new ArgumentException("content type must be application/json");

            var command = BindRequest<T>();
    
            return new CommandRequestHandler<T>(command,_bus,Request.Url);
        }

  


        protected T BindRequest<T>() where T : new()
        => Request.Body.Length == 0 && Request.Query == null
            ? new T()
            : this.Bind<T>(new BindingConfig());
        
    }

}

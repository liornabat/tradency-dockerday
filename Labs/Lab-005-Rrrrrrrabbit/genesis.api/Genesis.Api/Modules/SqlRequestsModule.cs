﻿using RawRabbit;
using Genesis.Shared.Commands;




namespace Genesis.Api.Modules
{
    public class SqlRequestsModule : ModuleBase
    {
        public SqlRequestsModule(IBusClient bus)
        : base(bus, "command-requests")
        {
            Get("", args => "This is Command request Api");

            Post("", async args =>
            {
                var response = await ForCommand<SendSomeCommand>()
               .SetValidationMethod(x => x.Validate())
               .HandleAsync();

                return response;

            });
        }

    }
  
}
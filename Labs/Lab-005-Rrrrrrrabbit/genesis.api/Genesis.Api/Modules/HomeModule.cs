﻿using RawRabbit;
using Genesis.Shared.Commands;





namespace Genesis.Api.Modules
{
    public class HomeModule : ModuleBase
    {
        public HomeModule(IBusClient bus)
        : base(bus, "")
        {
            Get("", args => "Welcome to Genesis Api !");
            
        }

    }
  
}
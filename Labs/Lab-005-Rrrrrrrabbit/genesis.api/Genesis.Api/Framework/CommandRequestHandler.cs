﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Genesis.Common.Validation;
using Genesis.Common.Commands;
using Genesis.Common.Extensions;
using Nancy.Responses.Negotiation;
using NLog;
using Genesis.Common.Services;
using RawRabbit;
using System.Collections.Generic;
using RawRabbit.Configuration.Publish;





namespace Genesis.Api.Framework
{
    public class CommandRequestHandler<T> where T : ICommand
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private T _command;
        private readonly IBusClient _bus;
        
        private Func<T, Task> _asyncCommandHandler = null;
        private Func<T, Task> _asyncOnSuccess = null;
        private Func<T, Task> _asyncOnError = null;
        private Func<T,IEnumerable<string>> _validationFunc = null;

        public CommandRequestHandler(T command, IBusClient bus, string Url)
        {

            _command = command;
            _bus = bus;
         
           
            
        }

        public CommandRequestHandler<T> Set(Action<T> action)
        {
            action(_command);

            return this;
        }


        public CommandRequestHandler<T> SetValidationMethod(Func<T,IEnumerable<string>> func)
        {
            _validationFunc = func;

            return this;
        }


        public CommandRequestHandler<T> ExecuteOnSuccess(Func<T, Task> func)
        {
            _asyncOnSuccess = func;
            return this;
        }

        public CommandRequestHandler<T> ExecuteOnError(Func<T, Task> func)
        {
            _asyncOnError = func;

            return this;
        }



        public async Task<object> HandleAsync()
        {
            object response = null;
            try
            {
                var validate = _validationFunc(_command).ToArray();
                if (validate.Any())
                {

                    _command.Request.Status = "Validation Error";
                    _command.Request.Errors = String.Concat(validate);
                    return  _command.Request;
                }
                var handler = new Handler();
                await handler
                   .Run(async () =>
                   {
                       Logger.Info("Sending Command: " +_command.Request.Id);
                       await _bus.PublishAsync(_command);
                       _command.Request.SetSendStatus();
                   })
                   .OnSuccess((Func<Task>)(async () =>
                   {

                       
                       
                       response =_command.Request;

                       if (_asyncOnSuccess != null)
                       {
                           await _asyncOnSuccess(_command);
                       }
                   }))

                   .OnError((Func<Exception, Logger, Task>)(async (ex, Logger) =>
                   {

                       _command.Request.SetErrorStatus("Error on Sending", ex.Message);
                     
                       response = _command.Request;

                       if (_asyncOnError != null)
                       {
                           await _asyncOnError(_command);
                       }

                      
                   }))

                   .Next()

                   .ExecuteAllAsync();

            }
            catch (Exception ex)
            {
                _command.Request.SetErrorStatus("Exception on Sending", ex.Message);
                response = _command.Request;
            }

            return response;
        }




    }
}

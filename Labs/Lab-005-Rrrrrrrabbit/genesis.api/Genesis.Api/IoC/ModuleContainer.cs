﻿using Autofac;
using Genesis.Api.IoC.Modules;

namespace Genesis.Api.IoC
{
    public class ModuleContainer : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<DispatcherModule>();
          
        }
    }
}
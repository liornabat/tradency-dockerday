dotnet restore
dotnet build -c Release
dotnet publish -c Release -o bin/Release/PublishOutput
docker build . -t liornabat/simple-web-application:latest
docker push liornabat/simple-web-application:latest

